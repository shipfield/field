#include "stdafx.h"
namespace MaidClass {

	class Maid{
		int money;
		int wool;
	public:
		Maid(int m);
		void takeWool();
		void buyAnimals();
		void sellWool();
		void feedDog();
	};
}