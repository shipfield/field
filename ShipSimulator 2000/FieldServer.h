#pragma once
#include "stdafx.h"
#include <iostream>
#include <vector>
#include <random>
#include <string>
using std::vector;

#include "Linlist.h"

using namespace llist;

namespace FieldClass{
	const int C_money = 200;
	const int C_woolCost = 20;
	const int C_hight = 30;
	const int C_width = 60;
	const int C_dogFoodCost = 10;
	const int C_shipCost = 30;
	const int C_dogCost = 50;
	const int C_maxShips=20;
	const int C_maxDogs=5;
	const int C_maxWolves = 5;
	const int C_grassSpeed=2;
	const int C_maxGrass = 50;
	const int C_decGrass = 5;

	class Field{
		struct area{
			int x;
			int y;
			bool ship;
			bool forest;
			bool wolf;
			bool dog;
			int grass;
			int client_id;
			area();
		};
		int allAnimals;
		int aliveAnimals;

		short hight; //������
		short width; //������

		short grassSpeed;
		short woolCost;
		short shipCost;
		short dogCost;
		short dogFoodCost;
	public:
		//����������
		List shp;
		List doge;
		List wlf;
		//��������� �� ����������
		List* all_ship;
		List* all_dogs;
		List* all_wolves;

		vector< vector< area > > fld_areas; //������������������ ������� ����������� ���� ���
		//public:
		Field(short w, short h); //+
		
		void placeAnimal(AnimalsClass::Animal *an, string type); //������ � ���� +
		void generateWolf(); //��������� � ������ 5 ���� +
		void kill(); //������� �������� � ������ +
		void decGrass(int x, int y); //���� ��� ����� +
		void incGrass(); //���� ����� +
		void rePlaceAnimal(AnimalsClass::Animal* an, int new_x, int new_y); //������, ��� � �������
		int numberOfThatAnimal(string type);
		int takeWool(); //���������� ����� � ������ � ������ �� ���� ������ +
		bool isDogAlive(); // +
		void ObjSearch(string type); //����� �� �������
		void addShip(); //+
		void addDog(); //+
		void wait(); //����� �������� ���� � ������ ������ (?)
		int gen(int min = 0, int max = 100); //+
		bool checkFree(int x, int y); // 1 - ��������, 0 - ������ //+
		void killSomeone(int id); //+
		
		//��������� �������� ��������
		AnimalsClass::Animal* findWithID(int id); //+
		bool parseMessage(string type, string action, int id); //+
		bool shipAction(string action,int id); //+
		bool wolfAction(string action, int id); //+
		bool dogAction(string action, int id); //+
		bool maidAction(string action);
	};

	
}