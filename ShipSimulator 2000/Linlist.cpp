#pragma once
#include "stdafx.h"
#include "Linlist.h"
using namespace std;
using namespace llist;

List::List(){
	head = NULL;
	cur = head;
	prev_cur = NULL;
}
bool List::isEmpty() const{
	return (!head);
}
int List::goNext(){
	if ((isEmpty()) || isEnd()) return 0;

	prev_cur = cur;
	cur = cur->next;
	if (isEnd()) return 0;
	return 1;
}
bool List::isEnd() const{
	return (!cur);
}
int List::insertAfter(GT* val){
	if (isEmpty()) return addFirst(val);
	if (isEnd()) return 0;

	Node* newElem = new Node;

	newElem->val = val;
	newElem->next = cur->next;
	cur->next = newElem;

	prev_cur = cur;
	cur = newElem;
	return 1;
}
int List::insertBefore(GT* val){
	if (isEmpty()) return 0;
	Node* newElem = new Node;

	newElem->val = val;
	newElem->next = cur;

	if (prev_cur)	prev_cur->next = newElem;
	else head = newElem;

	cur = newElem;
	return 1;

}
int List::goHead(){
	if (isEmpty()) return 0;
	cur = head;
	prev_cur = NULL;
	return 1;
}
//void List::displayList(){
//	if (isEmpty())
//	{
//		cout << "List is empty\n";
//		return;
//	}
//	Node* temp = head;
//
//	while (temp)
//	{
////		cout << temp->val << " ";
//		temp = temp->next;
//	}
//	cout << endl;
//}
int List::getValue(GT* val) const{
	if ((isEmpty()) || (isEnd())) return 0;
	val = cur->val;
	return 1;
}
int List::replace(GT* val){
	if ((isEmpty()) || (isEnd())) return 0;
	cur->val = val;
	return 1;
}
void List::delList(){
	Node* temp = head;
	head = NULL;
	while (temp)
	{
		Node* next = temp->next;
		delete temp;
		temp = next;
	}
}
int List::addFirst(GT* val){
	Node* newElem = new Node;
	newElem->val = val;
	newElem->next = head;
	head = newElem;
	cur = newElem;
	prev_cur = NULL;
	return 1;
}
int List::deleteElem(){
	if ((isEmpty()) || isEnd()) return 0;

	Node* temp = cur;

	if (!prev_cur){
		cur = cur->next;
		head = cur;
		delete temp;
		return 1;
	}
	cur = cur->next;
	prev_cur->next = cur;
	delete temp;
	return 1;
}
//int List::delVals(GT val){
//	if ((isEmpty()) || isEnd()) return 0;
//	int t = goHead();
//	bool f;
//	while (t, cur)
//	{
//		f = 0;
//		if (cur->val == val)
//		{
//			deleteElem();
//			f = 1;
//		}
//		if (!f) t = goNext();
//	}
//	t = goHead();
//	return 1;
//}
GT* List::getObj(){
	return cur->val;
}

