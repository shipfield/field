#include "stdafx.h"
#include <stdint.h>
#include <string>
using namespace std;
#include "FieldServer.h"
using namespace FieldClass;
using namespace AnimalsClass;

namespace clients {
	class Ship : public Animal{

		short wool;
	public:
		bool request(Field* s, string action, int id); //+
		void action(Field* s); //+
		void eatGrass(); //������, ������
		int giveWool(); //+
		Ship();
		~Ship();
	};


	class Wolf : public Animal {

	public:
		Wolf();
		bool request(Field* s, string action, int id); //+
		void action(Field* s); //+

	};

	class Dog : public Animal{

	public:
		Dog();
		bool request(Field* s, string action, int id); //+
		void action(Field* s); //+
	};
}