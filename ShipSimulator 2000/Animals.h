#pragma once
#include "stdafx.h"
#include <stdint.h>
#include <string>
using namespace std;

namespace AnimalsClass{

	class Animal{
		
	protected:
		short vision;
	    short satiety; //�������. ���� 0 - ������
		short age;
		string target; //???
		short coordX;
		short coordY;
		int ID;
		static int next_id;
	public:
		int getID(); //+
		Animal();
		void decAge(); //+
		void decSatiety(int a=1); //+
		void incSatiety(int a=1); //+ 
		bool isAlive(); //+
		void setXY(int x, int y); //+
		int getX(); //+
		int getY(); //+
	};


}