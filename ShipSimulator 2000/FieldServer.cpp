#include "stdafx.h"
#include "FieldServer.h"
#include "Ship.h"
using namespace FieldClass;

int AnimalsClass::Animal::next_id=1;
 
Field::area::area(){
	x = 0;
	y = 0;
	ship = 0;
	forest = 0;
	wolf = 0;
	grass = 50;
	dog = 0;
	client_id = -1;
}

Field::Field(short h, short w){
	 all_ship = &shp;
	 all_dogs = &doge;
	 all_wolves = &wlf;

	allAnimals = 0;
	aliveAnimals = 0;
	hight = h;
	width = w;
	grassSpeed = C_grassSpeed;
	woolCost=C_woolCost;
	shipCost=C_shipCost;
	dogCost=C_dogCost;
	dogFoodCost=C_dogCost;

	this->fld_areas = vector< vector < area > >(w, vector<area>(h, area()));

	for (int i = 0; i < w; i++)
		for (int j = 0; j < h; j++){
			//	fld_areas[i][j] = area();
			fld_areas[i][j].y = j;
			fld_areas[i][j].x = i;
		}
	for (int j = 0; j < h; j++){
		fld_areas[0][j].forest = 1;
		fld_areas[0][j].grass = 0;

		fld_areas[w - 1][j].forest = 1;
		fld_areas[w - 1][j].grass = 0;
	}

	for (int i = 0; i < w; i++){
		fld_areas[i][h - 1].forest = 1;
		fld_areas[i][h - 1].grass = 0;

		fld_areas[i][0].forest = 1;
		fld_areas[i][0].grass = 0;
	}
}
void Field::kill(){
	
	if (!all_ship->isEmpty()) {
		all_ship->goHead();
		while (!all_ship->isEnd()){
			AnimalsClass::Animal* tmp;
			tmp = all_ship->getObj();
			if (!tmp->isAlive()){
				all_ship->deleteElem();
			}
			all_ship->goNext();
		}
	}

	if (!all_dogs->isEmpty()) {
		all_dogs->goHead();
		while (!all_dogs->isEnd()){
			AnimalsClass::Animal *tmp;
			tmp = all_dogs->getObj();
			if (!tmp->isAlive()){
				all_dogs->deleteElem();
			}
			all_dogs->goNext();
		}
	}

	if (!all_wolves->isEmpty()) {
		all_wolves->goHead();
		while (!all_wolves->isEnd()){
			AnimalsClass::Animal* tmp;
			tmp = all_wolves->getObj();
			if (!tmp->isAlive()){
				all_wolves->deleteElem();
			}
			all_dogs->goNext();
		}
	}
}
void Field::addShip(){
	auto p = new clients::Ship;
	placeAnimal(p, "ship");
	all_ship->insertAfter(p);
}
void Field::wait(){
	kill();
	incGrass();
}
int Field::takeWool(){
	if (all_ship->isEmpty()) return 0;
	int money = 0;
	all_ship->goHead();
	while (!all_ship->isEnd()){
		clients::Ship* shp;
		shp = (clients::Ship*)all_ship->getObj();
		money += shp->giveWool();
		all_ship->goNext();
	}
	return money*woolCost;
}
bool Field::isDogAlive(){
	return (!all_dogs->isEmpty());
}

void Field::addDog(){
	auto d = new clients::Dog;
	placeAnimal(d, "dog");
	all_dogs->insertAfter(d);
}
void Field::incGrass(){
	for (int i = 1; i < width - 1; i++)
		for (int j = 1; i < hight - 1; j++){
			if (fld_areas[i][j].grass < C_maxGrass)
				fld_areas[i][j].grass += grassSpeed;

			if (fld_areas[i][j].grass > C_maxGrass)
				fld_areas[i][j].grass = C_maxGrass;		
		}
}
void Field::decGrass(int x, int y){
	if (fld_areas[x][y].grass > C_decGrass) fld_areas[x][y].grass -= C_decGrass;
	else fld_areas[x][y].grass = 0;
	
}
int Field::gen(int min, int max){
	random_device rd;
	mt19937 gen(rd());

	uniform_int_distribution<> distr(min, max);
	return distr(gen);
}
void Field::generateWolf(){
	int chance;
	chance = gen();
	if ((chance) < 6) {
		auto waf = new clients::Wolf;
		placeAnimal(waf, "wolf");
		all_wolves->insertAfter(waf);
	}
}
//�������� �� ��������� �����
bool Field::checkFree(int x, int y){
		return ((fld_areas[x][y]).client_id==-1);
}
//����������� �������� �� ��������� �����
void Field::placeAnimal(AnimalsClass::Animal *an, string type){
	
	if ((type == "ship")){  //|| (type == "dog")){
		bool scs = 0;
		int x;
		int y;
		while (!scs){
			x = gen(1, width - 2);
			y = gen(1, hight - 2);
			scs = checkFree(x, y);
		}
		if (type == "ship") fld_areas[x][y].ship = 1;
		//if (type == "dog")   fld_areas[x][y].dog = 1;
		an->setXY(x, y);
		fld_areas[x][y].client_id = an->getID(); //�����������
		return;
	}

	if ((type == "dog")){
		bool scs = 0;
		int x;
		int y;
		while (!scs){
			x = gen(1, width - 2);
			y = gen(1, hight - 2);
			scs = checkFree(x, y);
		}
		if (type == "dog")   fld_areas[x][y].dog = 1;
		an->setXY(x, y);
		fld_areas[x][y].client_id = an->getID(); //�����������
		return;
	}

	if (type == "wolf"){
		bool scs = 0;
		int x;
		int y;

		while (!scs){
			bool wher = gen(0, 1); //������������� ��� �����������
	
			if (!wher){ //�������������
				 x = gen(0, width-1);
				 y = gen(0, 1); //���� ��� ��� (1)
				 if (y) y = hight-1;
				scs = checkFree(x, y);
			}
			else { //�����������
				x = gen(0, 1); //����� ��� ����
				if (x) x = width - 1;
				
				y = gen(0, hight-1); 
				scs = checkFree(x, y);
			}
		
		}
		fld_areas[x][y].wolf = 1;
		an->setXY(x, y);
		fld_areas[x][y].client_id = an->getID(); //�����������

		return;
	}

}

//��������� ����������� ��������� 
bool Field::parseMessage(string type, string action, int id){
	if (type == "ship") return shipAction(action, id);
	if (type == "wolf") return wolfAction(action, id);
	if (type == "dog") return dogAction(action, id);
	if (type == "maid") return maidAction(action);
}
//����� ������� �� ��� ����������� ������
Animal* Field::findWithID(int id){
	int cur = id % 1000;
	if (cur == 1){
		all_ship->goHead();
		while (!all_ship->isEnd()){
			clients::Ship* shp;
			shp = (clients::Ship*)all_ship->getObj();
			if (id == shp->getID()) return shp;
			else all_ship->goNext();
		}
	}
	else if (cur == 3){
		all_dogs->goHead();
		while (!all_dogs->isEnd()){
			clients::Dog* d;
			d = (clients::Dog*)all_dogs->getObj();
			if (id == d->getID()) return d;
			else all_dogs->goNext();
		}
	}
	else if (cur == 2){
		all_wolves->goHead();
		while (!all_wolves->isEnd()){
			clients::Wolf* wf;
			wf = (clients::Wolf*)all_wolves->getObj();
			if (id == wf->getID()) return wf;
			else all_wolves->goNext();
		}
	}
}
//����������� �� �������� ������ �� ������-�� ����������� ����������
void Field::rePlaceAnimal(AnimalsClass::Animal* an, int new_x, int new_y){
	int prev_x = an->getX();
	int prev_y = an->getY();
	int type = an->getID() % 1000;
	an->setXY(new_x, new_y);
	if (type == 1) {
		fld_areas[prev_x][prev_y].ship = 0;
		fld_areas[prev_x][prev_y].client_id = -1;
		fld_areas[new_x][new_y].ship = 1;
		fld_areas[new_x][new_y].client_id = an->getID();
		return;
	}

	if (type == 2){
		fld_areas[prev_x][prev_y].wolf= 0;
		fld_areas[prev_x][prev_y].client_id = -1;
		fld_areas[new_x][new_y].wolf = 1;
		fld_areas[new_x][new_y].client_id = an->getID();
	}

	if (type == 3){
		fld_areas[prev_x][prev_y].dog = 0;
		fld_areas[prev_x][prev_y].client_id = -1;
		fld_areas[new_x][new_y].dog = 1;
		fld_areas[new_x][new_y].client_id = an->getID();
	}
}
//��������
void Field::killSomeone(int id){
	int type = id % 1000;
	switch (type){
	case 1:{
		while (!all_ship->isEnd()){
			clients::Ship* shp;
			shp = (clients::Ship*)all_ship->getObj();
			if (id == shp->getID()){
				int x, y;
				x = shp->getX();
				y = shp->getY();
				fld_areas[x][y].ship = 0;
				fld_areas[x][y].client_id = -1;
				all_ship->deleteElem();
				cout << "Ovtsa deleted, id=" << id << endl;
				return;
			}
		}
	}
	case 2:{
		all_wolves->goHead();
		while (!all_wolves->isEnd()){
			clients::Wolf* wf;
			wf = (clients::Wolf*)all_wolves->getObj();
			if (id == wf->getID()){
				int x, y;
				x = wf->getX();
				y = wf->getY();
				fld_areas[x][y].wolf = 0;
				fld_areas[x][y].client_id = -1;
				all_wolves->deleteElem();
				return;
			}
		}
	}
	case 3:{
		all_dogs->goHead();
		while (!all_dogs->isEnd()){
			clients::Dog* d;
			d = (clients::Dog*)all_dogs->getObj();
			if (id == d->getID()){
				int x, y;
				x = d->getX();
				y = d->getY();
				fld_areas[x][y].dog = 0;
				fld_areas[x][y].client_id = -1;
				all_dogs->deleteElem();
				return;
			}
		}
	}
}
}
//��������� �������� 
bool Field::shipAction(string action, int id){
	//��������� �������� 
	clients::Ship* tmp;
	tmp = (clients::Ship*)findWithID(id);
	int obx = tmp->getX();
	int oby = tmp->getY();
	if (action == "eat grass"){
		if (fld_areas[obx][oby].grass < C_decGrass) return false;
		decGrass(obx, oby);
		tmp->incSatiety(C_decGrass);
		cout << "Ovtsa poela\n";
		return true;
	}
	if (action == "move") //rePlaceAnimal(tmp);
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (!fld_areas[i][j].forest)
					if (checkFree(i,j))
						if (fld_areas[i][j].grass >= C_decGrass){
							rePlaceAnimal(tmp, i, j); //����������� �� ������ ��������� ����� ����
							return true;
						}
			}
	
}
bool Field::wolfAction(string action, int id){
	clients::Wolf* tmp;
	tmp = (clients::Wolf*)findWithID(id);
	int obx = tmp->getX();
	int oby = tmp->getY();

	if (action == "if there dog"){
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight) break;
				if (j > width) break;
				if (fld_areas[i][j].dog) return true;
			}
	}
	if (action == "run from dog"){
		int d_x, d_y;
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight-1) break;
				if (j > width-1) break;
				if (fld_areas[i][j].dog) {
					d_x = i; d_y = j;
				}
			}
		int w_x, w_y;
		w_x = obx - (d_x - obx);
		w_y = oby - (d_y - oby);
		if (w_x < 0 || w_y < 0 || w_x > hight - 1 || w_y > width - 1) return false; //���� �������������

		rePlaceAnimal(tmp, w_x, w_y); //�����
	}
	
	if (action == "if there ship"){
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight) break;
				if (j > width) break;
				if (fld_areas[i][j].ship) return true;
			}
	}

	if (action == "move to ship"){
		int s_x, s_y;
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight) break;
				if (j > width) break;
				if (fld_areas[i][j].ship) {
					s_x = i;
					s_y = j;
				}
			}
		rePlaceAnimal(tmp, s_x, oby); //�������
		return true;
	}
	
	if (action == "eat ship"){
		int s_ID, s_x, s_y;
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight) break;
				if (j > width) break;

				if (fld_areas[i][j].ship){
					s_ID = fld_areas[i][j].client_id; //��������� id ��������� ����	
					s_x = i; s_y = j;
				}
			}
		int koef = abs(obx - s_x) + abs(oby - s_y);
		if (koef == 2) return false;
		else {
			tmp->incSatiety(10);
			killSomeone(s_ID);
			return true;
		}
	}
	if (action == "move"){
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight) break;
				if (j > width) break;
				if (checkFree(i, j)) rePlaceAnimal(tmp, i, j);
				return true;
			}
	}
}
bool Field::dogAction(string action, int id){
	clients::Dog* tmp;
	tmp = (clients::Dog*)findWithID(id);
	int obx = tmp->getX();
	int oby = tmp->getY();

	if (action == "if there wolf"){
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight) break;
				if (j > width) break;
				if (fld_areas[i][j].wolf) return true;
			}
	}
	if (action == "kill"){
		int w_ID, w_x, w_y;
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight) break;
				if (j > width) break;

				if (fld_areas[i][j].wolf){
					w_ID = fld_areas[i][j].client_id; //��������� id ��������� ����	
					w_x = i; w_y = j;
				}
			}
		int koef = abs(obx - w_x) + abs(oby - w_y);
		if (koef == 2) return false;
		else{
			killSomeone(w_ID);
			return true;
		}
	}

	if (action == "go to wolf"){
		int w_x, w_y;
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight) break;
				if (j > width) break;
				if (fld_areas[i][j].wolf) {
					w_x = i;
					w_y = j;
				}
			}
		rePlaceAnimal(tmp, w_x, oby); //�������
		return true;
	}
	if (action == "move"){
		for (int i = obx - 1; i <= obx + 1; i++)
			for (int j = oby - 1; j <= oby + 1; j++){
				if (i < 0) i = 0;
				if (j < 0) j = 0;
				if (i > hight) break;
				if (j > width) break;
				if (checkFree(i, j)) rePlaceAnimal(tmp, i, j);
				return true;
			}
	}
}
//�������
bool Field::maidAction(string action){
	return 1;

}