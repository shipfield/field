#include "stdafx.h"
#include "Animals.h" 
using namespace std;

typedef AnimalsClass::Animal GT;

namespace llist {
//	template <class GT>
	class List {
		struct Node{
			Node* next;
			GT* val;
		};
		
		Node* head;
		Node* cur;
		Node* prev_cur;
	public:
		List();
		bool isEmpty() const;
		int goHead();
		int goNext();
		bool isEnd() const;
		int getValue(GT* val) const;
		GT* getObj();
		int deleteElem();
		int insertBefore(GT* val);
		int insertAfter(GT* val);
		int replace(GT* val);
		void delList();
		void displayList();
		int addFirst(GT* val);
		int delVals(GT* val);

	};
}