#include "stdafx.h"
#include <iostream>
#include "Ship.h"
using namespace clients;

//DOGE
Dog::Dog() : Animal(){
	target = "wolf";
	age = 25;
	ID = ID + 3000;
}
//������ � ������ ���� �������� �� ���� "���"
bool Dog::request(Field* s, string action, int id){
	return (s->parseMessage("dog", action, ID));
}
void Dog::action(Field* s){
	bool index;
	index = request(s, "if there wolf", ID);
	if (index) index = request(s, "kill", ID);
	if (index) return;
	else {
		request(s, "go to wolf", ID);
		return;
	}
	request(s, "move", ID);
}
//WOLF
Wolf::Wolf() : Animal(){
	age = 30;
	target = "ship";
	ID = ID + 2000;
}
bool Wolf::request(Field* s, string action, int id){
	return (s->parseMessage("wolf", action, ID));
}
void Wolf::action(Field* s){
	bool index;
	index = request(s, "if there dog", ID);
	if (index) {
		request(s, "run from dog", ID);
		return;
	}

	index = request(s, "if there ship", ID);
	
	if (index){
	index =	request(s, "eat ship", ID);
		return; 
	}
	else {
		request(s, "move to ship", ID);
		return;
	}

	request(s, "move", ID);
	return;
}

//SHIP
Ship::Ship() : Animal(){
	wool = 0;
	//maxWool = 50;
	ID = ID + 1000;
}
int Ship::giveWool(){
	int w;
	w = wool;
	wool = 0;
	return w;
}
Ship::~Ship(){
	
}
bool Ship::request(Field* s, string action, int id){
	return (s->parseMessage("ship", action, ID));
}
void Ship::action(Field* s){
	bool index;
	index = request(s, "eat grass", ID);
	if (index) return;
	
	index = request(s, "move", ID);
	if (index) return;

	return;
}