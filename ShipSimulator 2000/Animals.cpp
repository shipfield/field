#include "stdafx.h"
#include "Animals.h"
using namespace AnimalsClass;
using namespace std;


Animal::Animal(){
	vision = 4;
	satiety = 50;
	age = 20;
	target = "";
	ID = next_id;
	next_id++;
	//	maxSatiety = 50;
}
void Animal::decSatiety(int a ){
	satiety-=a;
	if (satiety < 0) satiety = 0;
}
void Animal::incSatiety(int a ){
	if (satiety<50) satiety+=a;
}
void Animal::decAge(){
	age--;
}
bool Animal::isAlive(){
	return ((satiety) && (age));
}
void Animal::setXY(int x, int y){
	coordX = x;
	coordY = y;
}
int Animal::getID(){
	return ID;
}
int Animal::getX(){
	return coordX;
}
int Animal::getY(){
	return coordY;
}



